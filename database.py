import sys
from os import listdir
from os.path import isfile, join, isdir
from pymongo import MongoClient
from pymongo.errors import ConnectionFailure
from numpy import loadtxt, int64

from settings import *


class Database(object):

    def __init__(self):
        try:
            self.client = MongoClient(DATABASE_SERVER, DATABASE_PORT)
        except ConnectionFailure:
            sys.stderr.write("E> Database unreachable.\n")
            sys.exit(2)

        self.db = self.client[DATABASE_NAME]
        self.reference = self.db['reference']

    def import_files(self, path):

        dirs = [f for f in listdir(path) if isdir(join(path, f))]

        for directory in dirs:
            full_path = join(path, directory)
            files_in_dir = [f for f in listdir(full_path) if isfile(join(full_path, f))]

            for filename in files_in_dir[:1]:
                record = {'user': filename.split('.')[0]}
                filepath = join(full_path, filename)
                data = loadtxt(filepath, dtype=int64, comments='E')
                record['data'] = data.tolist()

                sys.stdout.write("I> File saved with id %s.\n" % self.reference.insert(record))

    def num_of_records(self):
        return self.reference.count()

    def get_all_records(self):
        return self.reference.find()

    def close(self):
        self.client.disconnect()


