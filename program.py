import sys
import os
from optparse import make_option, OptionParser

from comparator import Compare
from database import Database
from structs import Collection, Record
from tests import Test


if __name__ == "__main__":

    option_list = (
        make_option('--loaddata', '-l', dest='data_path', action='store'),
        make_option('--identify', '-i', dest='identify', action='store'),
        make_option('--tests', '-t', dest='tests', action='store'),
    )

    parser = OptionParser(option_list=option_list)
    (options, args) = parser.parse_args()

    # create database connection
    db = Database()

    # import files into database
    if options.data_path:
        path = options.data_path
        if os.path.isdir(path):
            db.import_files(options.data_path)
        else:
            sys.stderr.write("E> Directory does not exists.\n")
        sys.stdout.write("I> Database contains %s objects.\n" % db.num_of_records())

    if options.tests:
        path = options.tests
        if os.path.isdir(path):
            # run
            Test(db, path)
        else:
            sys.stderr.write("E> Directory does not exists.\n")

    if options.identify:
        filepath = options.identify
        if os.path.exists(filepath):

            # create collection of database records
            collection = Collection(db.get_all_records())

            f = Record(filepath=filepath)

            d = Compare(collection, f)
            user = d.run()

            sys.stdout.write("Match user: %s.\n" % user)

        else:
            sys.stderr.write("E> File does not exists.\n")

    # disconnect from database
    db.close()

    sys.exit(0)