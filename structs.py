import os
import sys
from numpy import array as np_array, append as np_append, int64, loadtxt


class Record(object):

    _name = None
    _features = dict()

    def __init__(self, *args, **kwargs):
        if "filepath" in kwargs:
            self._from_file(kwargs.get('filepath'))

    def __str__(self):
        return "Record: %s" % self._name

    def features(self):
        return self._features

    def name(self):
        return self._name

    def set_feature(self, item):
        self._features = item

    def set_name(self, name):
        self._name = name

    def _from_file(self, filepath):
        head, tail = os.path.split(filepath)
        self._name = tail.split('.')[0]
        self._features = loadtxt(filepath, dtype=int64, skiprows=4, comments='E')


class Collection(object):

    _records = np_array([], dtype=int)

    def __init__(self, db_records):
        sys.stdout.write("I> Loading collection...\n")
        for item in db_records:
            rec = Record()
            rec.set_feature(np_array(item.get('data'), dtype=int64))
            rec.set_name(item.get('user'))
            self._add(rec)

    def __iter__(self):
        for item in self._records:
            yield item

    def __len__(self):
        return len(self._records)

    def _add(self, item):
        self._records = np_append(self._records, item)
