from os.path import join, isdir, isfile
from os import listdir
import sys

from structs import Collection, Record
from comparator import Compare


class Test(object):

    def __init__(self, db, path):

        ok = 0
        fail = 0

        collection = Collection(db.get_all_records())

        dirs = [f for f in listdir(path) if isdir(join(path, f))]

        for directory in dirs[150:]:
            full_path = join(path, directory)
            files_in_dir = [f for f in listdir(full_path) if isfile(join(full_path, f))]

            for filename in files_in_dir[:1]:
                filepath = join(full_path, filename)

                f = Record(filepath=filepath)

                d = Compare(collection, f)
                user = d.run()
                if user[:5] == filename[:5]:
                    ok += 1
                else:
                    fail += 1
        sys.stdout.write("I> Count of incorrect match: %s.\n" % fail)
        sys.stdout.write("I> Count of correct match: %s.\n" % ok)


