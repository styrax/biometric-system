# README #

Simple biometric system for signature recognition written in Python. Signatures were taken from open database at biometrics.idealtest.org. Recognition is based on DTW (dynamic time warping) algorithm.