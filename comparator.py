import sys
from mlpy import PCA, dtw_std as dtw

from settings import COLS


class Compare(object):

    def __init__(self, collection, record):

        self.collection = collection
        self.record = record

    def run(self):
        minimum = sys.maxint
        user = None
        for c in self.collection:
            num = self.compare(c, self.record)
            if num < minimum:
                minimum = num
                user = c.name()
        return user

    def compare(self, p1, p2):

        r_p1 = self._reduce_dimension(p1.features().T[COLS].T)
        r_p2 = self._reduce_dimension(p2.features().T[COLS].T)

        rr_p1 = [x[0] for x in r_p1.tolist()]
        rr_p2 = [x[0] for x in r_p2.tolist()]

        dist = dtw(rr_p1, rr_p2, dist_only=True)

        return dist


    @staticmethod
    def _reduce_dimension(array):
        pca = PCA()
        pca.learn(array)
        return pca.transform(array, k=1)


